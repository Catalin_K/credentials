﻿using CredentialsService.Protos;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CredentialsService.Services
{
    public class CredentialsService : Credentials.CredentialsBase
    {

        public override Task<GetPersonResponse> GetPerson(GetPersonRequest request, ServerCallContext context)
        {

            Console.WriteLine("Nume: " + request.Person.Nume);
            Console.WriteLine("CNP: " + request.Person.Cnp);
            Int32 bdayYear = 0;
            if (request.Person.Cnp.StartsWith('1'))
            {
                request.Person.Gender = Person.Types.Gender.Male;
                bdayYear = 1900 + Int32.Parse(request.Person.Cnp.Substring(1, 2));
            }
            else if (request.Person.Cnp.StartsWith('2'))
            {
                request.Person.Gender = Person.Types.Gender.Female;
                bdayYear = 1900 + Int32.Parse(request.Person.Cnp.Substring(1, 2));
            }
            else if (request.Person.Cnp.StartsWith('5'))
            {
                request.Person.Gender = Person.Types.Gender.Male;
                bdayYear = 2000 + Int32.Parse(request.Person.Cnp.Substring(1, 2));
            }
            else if (request.Person.Cnp.StartsWith('6'))
            {
                request.Person.Gender = Person.Types.Gender.Female;
                bdayYear = 2000 + Int32.Parse(request.Person.Cnp.Substring(1, 2));
            }
            Console.WriteLine("Gender:" + request.Person.Gender);

            var currentTime = DateTime.Today;
            var age = currentTime.Year - bdayYear;

            request.Person.Age = age;
            Console.WriteLine("Age:" + request.Person.Age);
            return Task.FromResult(new GetPersonResponse
            {
                //Message = "Name:" + request.Person.Nume+"\nCNP:"+request.Person.Cnp
                Message = "You succesfully logged into the server!"
            });


        }
    }
}
