﻿using CredentialsClient.Protos;
using Grpc.Net.Client;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CredentialsClient1
{
    class Program
    {

        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Credentials.CredentialsClient(channel);
            try
            {
                Console.Write("Nume:");
                string nume = Console.ReadLine();
                if (nume.Any(char.IsDigit) is true)
                {
                    throw new Exception("Numele nu poate contine cifre!");
                }

                Console.Write("CNP:");
                string cnp = Console.ReadLine();
                
                if (cnp.Length != 13 && cnp.All(char.IsDigit) is false)
                {
                    throw new Exception("CNP-ul trebuie sa fie un numar format din 13 cifre!");
                }

                var personToBeAdded = new Person() { Nume = nume, Cnp = cnp };

                if (personToBeAdded is null)
                {
                    throw new Exception("Datele nu au fost initializate corect!"); 
                }
                var reply = await client.GetPersonAsync(new GetPersonRequest { Person = personToBeAdded });
               
                Console.WriteLine(reply.Message);
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }
    }
}
